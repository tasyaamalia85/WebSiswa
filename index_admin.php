<?php
session_start();
if(!$_SESSION['login']){
	header('location:login_admin.php');
}
?>
<html>
	<head><title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
	<div class="menu" align="align">
		<ul>
			<li><a href="index_admin.php">Home</a></li>
			<li><a href="logout_admin.php">Logout</a></li>
		</ul>
	</div>
	<div class="headeradmin">
	</div>
	<div class="kontenadmin"><br><h1 align="center">Data Siswa</h1><br><br>

<center>
<table  align="left" class="data" border="5" cellpadding="5" cellspacing="0">
	<tr bgcolor="lightseagreen">
		<th >No</th>
		<th>NIS</th>
		<th>Nama</th>
		<th>Alamat</th>
		<th>No Telp</th>
		<th>Gender</th>
		<th>Agama</th>
		<th>Tempat Lahir</th>
		<th>Tanggal Lahir</th>
		<th>Username</th>
		<th>Password</th>
		<th>Aksi</th>
	</tr>
	<?php
	include "koneksi.php";
	$sql_data_siswa=mysqli_query($konek,"SELECT * FROM `data_siswa`");
	$no=0;
	while($data_siswa=mysqli_fetch_array($sql_data_siswa)){
		$no++;
	
	echo "<tr>
		<td>$no</td>
        <td>".$data_siswa['induk_siswa']."</td>
		<td>".$data_siswa['nama_siswa']."</td>
		<td>".$data_siswa['tempat_tinggal']."</td>
		<td>".$data_siswa['telp']."</td>
		<td>".$data_siswa['gender']."</td>
		<td>".$data_siswa['agama']."</td>
		<td>".$data_siswa['tempat_lahir']."</td>
		<td>".$data_siswa['tanggal_lahir']."</td>
        <td>".$data_siswa['username_siswa']."</td>
        <td>".$data_siswa['password']."</td>
		<td><a href='ubah.php?induk_siswa=".$data_siswa['induk_siswa']."'>UBAH</a>
		<a href='hapus_admin.php?induk_siswa=".$data_siswa['induk_siswa']."'>HAPUS</a></td>
	</tr>";
	}
	?>
</table>
</center>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<h1 align="center">Data Admin</h1><br><br>
<center>
<table  align="left" class="data" border="5" cellpadding="5" cellspacing="0" width="60%">
	<tr bgcolor="lightseagreen">
		<th >No</th>
		<th>ID</th>
		<th>Nama</th>
		<th>Username</th>
		<th>Password</th>
		<th>Aksi</th>
	</tr>
	<?php
	include "koneksi.php";
	$sql_data_admin=mysqli_query($konek,"SELECT * FROM `admin`");
	$no=0;
	while($data_admin=mysqli_fetch_array($sql_data_admin)){
		$no++;
	
	echo "<tr>
		<td>$no</td>
		<td>".$data_admin['id_admin']."</td>
		<td>".$data_admin['nama_admin']."</td>
		<td>".$data_admin['username']."</td>
        <td>".$data_admin['password']."</td>
		<td><a href='ubah_admin.php?id_admin=".$data_admin['id_admin']."'>UBAH</a>
		<a href='hapus_admin2.php?id_admin=".$data_admin['id_admin']."'>HAPUS</a></td>
	</tr>";
	}
	?>
</table>
</center>
</div>